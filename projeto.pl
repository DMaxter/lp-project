% Daniel Maximiano Matos 89429

%*************************************************************************************************
% Predicado `propaga`/3
%	propaga(Puzzle, Posicao, Posicoes)
%
%	Retorna a lista com as posicoes a serem preenchidas, para Pos ser preenchida
%*************************************************************************************************
propaga([Primeiro | _], Posicao, Posicoes):- 
	selTermometro(Primeiro, Posicao, Todas), 
	cortaListaElem(Todas, Posicao, Desordenado), 
	sort(Desordenado, Posicoes).

%*************************************************************************************************
% Predicado `selTermometro`/3
%	selTermometro(Puzzle, Posicao, Posicoes)
%
% 	Seleciona o termometro onde esta contida a Posicao
%*************************************************************************************************
selTermometro([Primeiro | _], Posicao, Primeiro):- member(Posicao, Primeiro).
selTermometro([_ | Resto], Posicao, Lista):- selTermometro(Resto, Posicao, Lista).

%*************************************************************************************************
% Predicado `cortaListaElem`/3
%	cortaListaElem(Lista, Elemento, Corte)
%
%	Reduz o tamanho de Lista ate a Elemento
%*************************************************************************************************
cortaListaElem([Primeiro | _], Primeiro, [Primeiro]).
cortaListaElem([Primeiro | Resto], Elemento, [Primeiro | Resto1]):- cortaListaElem(Resto, Elemento, Resto1).

%*************************************************************************************************
% Predicado `nao_altera_linhas_anteriores`/3
%	nao_altera_linhas_anteriores(Posicoes, Linha, Ja_Preenchidas)
%
%	Verifica se nao necessario recorrer a linhas anteriores a Linha para
%	preencher os elementos de Posicoes
%*************************************************************************************************
nao_altera_linhas_anteriores([], _, _).
nao_altera_linhas_anteriores([Primeiro | Resto], Linha, Preenchidas):- 
	member(Primeiro, Preenchidas),
	nao_altera_linhas_anteriores(Resto, Linha, Preenchidas).

nao_altera_linhas_anteriores([Primeiro | Resto], Linha, Preenchidas):-
	primElemTuplo(Primeiro, Elemento),
	Elemento >= Linha,
	nao_altera_linhas_anteriores(Resto, Linha, Preenchidas).



%*************************************************************************************************
% Predicado `verifica_parcial`/4
%	verifica_parcial(Puzzle, Ja_Preenchidas, Dimensao, Possibilidades)
%
%	Verifica se uma possibilidade de preencher uma linha nao viola os totais das colunas
%*************************************************************************************************
verifica_parcial(Puzzle, Preenchidas, Dimensao, Possibilidades):-
	listaLimCol(Puzzle, Colunas),
	% Remover da lista de possibilidades as posicoes ja preenchidas
	remElemComum(Possibilidades, Preenchidas, PossSemPreenchidas),
	verificar(Colunas, Preenchidas, Dimensao, PossSemPreenchidas).

verificar(_, _, 0, _).
verificar(LimCol, Preenchidas, Coluna, Possibilidades):-
	Coluna > 0,
	% Obter limite para a coluna atual
	nth1(Coluna, LimCol, Max_inicial),
	% Verificar quantas posicoes preenchidas ocupam a coluna atual
	contaColRest(Preenchidas, Coluna, Max_inicial, Max_restante),
	% Verificar se as posicoes em Possibilidades nao ultrapassam o maximo para as coluna
	contaColRest(Possibilidades, Coluna, Max_restante, Restante),
	Restante >= 0,
	Colm1 is Coluna-1,
	verificar(LimCol, Preenchidas, Colm1, Possibilidades). 

%*************************************************************************************************
% Predicado `remElemComum`/3
%	remElemComum(Lista1, Lista2, Sem_Repetidos)
%
%	Retorna Lista1 sem os elementos contidos em Lista2
%*************************************************************************************************
remElemComum([], _, []).
remElemComum([Primeiro | Resto], Lista2, Final):-
	member(Primeiro, Lista2),
	remElemComum(Resto, Lista2, Final).
remElemComum([Primeiro | Resto], Lista2, Final):-
	\+ member(Primeiro, Lista2),
	remElemComum(Resto, Lista2, Intermedia),
	append([Primeiro], Intermedia, Final).

%*************************************************************************************************
% Predicado `contaColRest`/4
%	contaColRest(Lista, Coluna, Maximo, Restante)
%
%	Verifica se ainda existem posicoes a preencher para completar a coluna
%*************************************************************************************************
contaColRest([], _, Maximo, Maximo).
contaColRest([Primeiro | Resto], Coluna, Maximo, Max_restante):- 
	segElemTuplo(Primeiro, Coluna),
	contaColRest(Resto, Coluna, Maximo, Restante1),
	Restante1 > 0,
	Max_restante is Restante1 - 1.
contaColRest([Primeiro | Resto], Coluna, Maximo, Restante):-
	segElemTuplo(Primeiro, Elemento),
	Coluna \= Elemento,
	contaColRest(Resto, Coluna, Maximo, Restante).

%*************************************************************************************************
% Predicado `possibilidades_linha`/5
%	possibilidades_linha(Puzzle, Posicoes, Total, Preenchidas, Possibilidades)
%
%	Devolve as possibilidades para preencher a linha
%*************************************************************************************************
possibilidades_linha(_, _, 0, _, [[]]).
possibilidades_linha(Puzzle, Posicoes, Total, Preenchidas, Possibilidades):-
	% Gera todas as combinacoes de posicoes possiveis, N a N (com N = Total)
	findall(Combinacao, combinacoes(Total, Posicoes, Combinacao), Combinacoes),
	% De todas as combinacoes seleciona as que ja tem posicoes preenchidas para a linha atual
	% Se nao estiver nenhuma preenchida, devolve todas as combinacoes
	verificaPreenchidas(Combinacoes, Preenchidas, CombValidas),
	possibilidades(Puzzle, Posicoes, Total, Preenchidas, CombValidas, Desordenado),
	sort(Desordenado, Possibilidades),
	!.

possibilidades(Puzzle, Posicoes, Total, Preenchidas, Combinacoes, Possibilidades):-
	% Seleciona uma das combinacoes
	member(Elemento, Combinacoes),
	% Remove a combinacao da lista
	remElemComum(Combinacoes, [Elemento], Resto),
	% Aplica propaga a todos os elementos da lista
	propagaLista(Puzzle, Elemento, Propagado),
	% Verifica se nao foram introduzidos novos elementos na linha
	linha(Elemento, Linha),
	mantemElemLinha(Propagado, Linha, Lista1Linha),
	Lista1Linha = Elemento,
	% Realiza outras verificacoes para saber se a combinacao pode ser parte da solucao do puzzle
	nao_altera_linhas_anteriores(Propagado, Linha, Preenchidas),
	dimensao(Puzzle, Dimensao),
	verifica_parcial(Puzzle, Preenchidas, Dimensao, Propagado),
	!,
	possibilidades(Puzzle, Posicoes, Total, Preenchidas, Resto, PossInter),
	append(PossInter, [Propagado], Possibilidades).
possibilidades(_, _, _, _, _, []).

%*************************************************************************************************
% Predicado `combinacoes`/3
%	combinacoes(N, Lista, Combinacoes)
%
%	Devolve uma lista (Combinacoes) com a combinacao de Lista, N a N elementos
%*************************************************************************************************
combinacoes(0, _, []).
combinacoes(N, Lista, [Elemento | Resto]):-
	N > 0,
	append(_, [Elemento | ListaInter], Lista),
	Nm1 is N - 1,
	combinacoes(Nm1, ListaInter, Resto).

%*************************************************************************************************
% Predicado `mantemElemLinha`/3
%	mantemElemLinha(ListaLinhas, Linha, Lista1Linha)
%
%	Retorna ListaLinhas so com os elementos com linha igual a Linha
%*************************************************************************************************
mantemElemLinha([], _, []).
mantemElemLinha([Primeiro | Resto], Linha, Lista):-
	primElemTuplo(Primeiro, LinhaElem),
	LinhaElem = Linha,
	mantemElemLinha(Resto, Linha, ListaInter),
	append([Primeiro], ListaInter, Lista).
mantemElemLinha([Primeiro | Resto], Linha, Lista):-
	primElemTuplo(Primeiro, LinhaElem),
	LinhaElem \= Linha,
	mantemElemLinha(Resto, Linha, Lista).

%*************************************************************************************************
% Predicado `propagaLista`/3
%	propagaLista(Puzzle, Lista, Propagacao)
%
%	Devolve a lista que resulta de aplicar `propaga` a todos os elementos de Lista
%*************************************************************************************************
propagaLista(_, [], []).
propagaLista(Puzzle, [Primeiro | Resto], Propagacao):-
	propaga(Puzzle, Primeiro, Propagado),
	propagaLista(Puzzle, Resto, PropagacaoInter),
	append(Propagado, PropagacaoInter, Desordenado),
	sort(Desordenado, Propagacao).

%*************************************************************************************************
% Predicado `verificaPreenchidas`/3
%	verificaPreenchidas(Lista, Preenchidas, Resultado)
%
%	Se Preenchidas nao tiver elementos da mesma linha que lista, devolve Lista
%	Caso contrario devolve os elementos de Lista que contem os elementos de Preenchidas
%*************************************************************************************************
verificaPreenchidas(Lista, [], Lista).
verificaPreenchidas([], _, []).
verificaPreenchidas([Primeiro | Resto], Preenchidas, Resultado):-
	linha(Primeiro, Linha),
	mantemElemLinha(Preenchidas, Linha, PosLinha),
	PosLinha = [],
	Resultado = [Primeiro | Resto].
verificaPreenchidas([Primeiro| Resto], Preenchidas, Resultado):-
	linha(Primeiro, Linha),
	mantemElemLinha(Preenchidas, Linha, Lista2),
	verificaPreenchidas(Resto, Preenchidas, ResInter),
	(forall(member(Elemento, Lista2), member(Elemento, Primeiro))->
	append([Primeiro], ResInter, Resultado);
	Resultado = ResInter).

%*************************************************************************************************
% Predicado `resolve`/2
%	resolve(Puzzle, Solucao)
%
%	Devolve a solucao de Puzzle
%*************************************************************************************************
resolve(Puzzle, Solucao):-
	dimensao(Puzzle, Dimensao),
	resolveRecur(Puzzle, 1, Dimensao, [], Desordenada),
	sort(Desordenada, Solucao).

resolveRecur(_, Linha, Dimensao, Preenchidas, Desordenada):-
	Linha > Dimensao,
	Desordenada = Preenchidas.
resolveRecur(Puzzle, Linha, Dimensao, Preenchidas, Desordenada):-
	Linha =< Dimensao,
	% Gera todas as posicoes da linha atual
	geraPosLinha(Linha, Dimensao, Posicoes),
	listaLimLin(Puzzle, Limites),
	nth1(Linha, Limites, Total),
	possibilidades_linha(Puzzle, Posicoes, Total, Preenchidas, Possibilidades),
	!,
	% Seleciona uma das possibilidades e passa para a linha seguinte
	member(Elemento, Possibilidades),
	LinhaM1 is Linha+1,
	union(Preenchidas, Elemento, PreenchInter),
	resolveRecur(Puzzle, LinhaM1, Dimensao, PreenchInter, Desordenada).

%*************************************************************************************************
% Predicado `geraPosLinha`/3
%	geraPosLinha(Linha, Dimensao, Lista)
%
%	Devolve uma lista com todas as posicoes para Linha, ate a posicao (Linha, Dimensao)
%*************************************************************************************************
geraPosLinha(Linha, Dimensao, Lista):-
	geraPosRecur(Linha, 1, Dimensao, Lista).

geraPosRecur(Linha, Dimensao, Dimensao, [(Linha, Dimensao)]).
geraPosRecur(Linha, Coluna, Dimensao, Lista):-
	ColM1 is Coluna+1,
	geraPosRecur(Linha, ColM1, Dimensao, ListaInter),
	append([(Linha, Coluna)], ListaInter, Lista).

%*************************************************************************************************
% Predicado `primElemTuplo`/2
%	primElemTuplo(Tuplo, Elemento)
%
% 	Seleciona o primeiro elemento de Tuplo
%*************************************************************************************************
primElemTuplo((Primeiro, _), Primeiro).

%*************************************************************************************************
% Predicado `segElemTuplo`/2
%	segElemTuplo(Tuplo, Elemento)
%
% 	Seleciona o segundo elemento de Tuplo
%*************************************************************************************************
segElemTuplo((_, Segundo), Segundo).

%*************************************************************************************************
% Predicado `dimensao`/2
%	dimensao(Puzzle, Dimensao)
%
%	Devolve a dimensao de Puzzle
%*************************************************************************************************
dimensao(Puzzle, Dimensao):-
	listaLimLin(Puzzle, Limites),
	length(Limites, Dimensao).

%*************************************************************************************************
% Predicado `listaLimLin`/2
%	limitesLin(Puzzle, Limites)
%
%	Devolve a lista com os limites das linhas
%*************************************************************************************************
listaLimLin([_, Linhas, _], Linhas). 

%*************************************************************************************************
% Predicado `listaLimCol`/2
%	limitesLin(Puzzle, Limites)
%
%	Devolve a lista com os limites das colunas
%*************************************************************************************************
listaLimCol([_, _, Colunas], Colunas).

%*************************************************************************************************
% Predicado `linha`/2
%	linha(Lista, Linha)
%
%	Dada Lista, devolve a linha da primeira posicao
%*************************************************************************************************
linha([Primeiro | _], Linha):-primElemTuplo(Primeiro, Linha).
